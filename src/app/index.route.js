export function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
        url: '/',
        templateUrl: 'app/topFilms/top.films.html',
        controller: 'TopFilmsController',
        controllerAs: 'topFilms'
      }
    )

    .state('chart', {
      url: '/chart',
      templateUrl: 'app/chart/chart.html',
      controller: 'ChartController',
      controllerAs: 'chart'
    })

    .state('favorite', {
      url: '/favorite',
      templateUrl: 'app/favorite/favorite.html',
      controller: 'FavoriteController',
      controllerAs: 'favorite'
    });

  $urlRouterProvider.otherwise('/');
}
