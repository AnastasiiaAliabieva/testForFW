export class FavoriteController {
  constructor ($scope) {
    'ngInject';
    $scope.hello = 'favorite page is here';
    $scope.arrfilms = localStorage.getItem('filmsArr');

    $scope.deleteFromFavorite = function(film) {
      let index = $scope.arrfilms.indexOf(film);
      $scope.arrfilms.splice(index, index);
    }

  }


}
