export class ChartController {
  constructor($scope, $http) {
    'ngInject';
    $scope.hello = 'chart page is here';
    $scope.data = [];
    $scope.labels = [];
    $http.get('http://www.myapifilms.com/imdb/top?start=1&end=20&token=ebfb7525-3b98-4d85-b1ed-e1796c3b0087&data=1', {}).success(function (response) {
      let res = response.data.movies;
      res = res.sort((a, b) => {
        return (+a.year) - (+b.year);
      });

      $scope.startYear = Math.floor(res[0].year / 100) * 100;
      $scope.endYear = Math.round(res[res.length - 1].year / 100) * 100;

      for (let i = $scope.startYear; i < $scope.endYear; i += 30) {
        let currentStart = i;
        let currentEnd = i + 30;
        $scope.labels.push(' ' + currentStart + ' - ' + currentEnd)
        $http.get('http://www.myapifilms.com/imdb/top?start=' + currentStart + '&end=' + currentEnd + '&token=ebfb7525-3b98-4d85-b1ed-e1796c3b0087&format=json&data=0', {}).success(function (response) {
          $scope.data.push(response.data.movies.length);
        });
      }
    });

    $scope.colors = ['(52, 133, 214)', '(52, 85, 214)', '(52, 55, 214)'];
  }
}
