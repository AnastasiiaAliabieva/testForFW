export class TopFilmsController {
  constructor ($scope, $http) {
    'ngInject';
    $scope.linkToAuthor = 'http://www.imdb.com/name/';

    // localStorage.clear();

    $http.get('http://www.myapifilms.com/imdb/top?start=1&end=20&token=ebfb7525-3b98-4d85-b1ed-e1796c3b0087&data=1', {
    }).success(function (response) {
       $scope.films = response.data.movies;
    });

    $scope.addToFavorite = function(film) {

      let itemsInLocalStore = [];
      localStorage.getItem('filmsArr') ?  itemsInLocalStore = JSON.parse(localStorage.getItem('filmsArr')) : false;
      if(itemsInLocalStore.indexOf(film.idIMDB) === -1 ) itemsInLocalStore.push(film);
      localStorage.setItem('filmsArr', JSON.stringify(itemsInLocalStore));
    }

  }


}
